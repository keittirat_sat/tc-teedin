<?php get_header(); ?>

<h1 class="blue_ribbon_heaeder txt_center supermarket"><?php echo single_cat_title(); ?><img src="<?php bloginfo('template_directory'); ?>/img/quote_arrow.png" class="drop_arrow"></h1>

<div class="container" style="margin: 50px auto 40px;">
    <div class="row">
        <div class="col-xs-10 col-xs-offset-1">
            <?php while (have_posts()): the_post(); ?>
              <div class="row" style="margin-bottom: 20px;">
                  <?php $thumb_id = get_post_thumbnail_id(); ?>
                  <?php if ($thumb_id): ?>
                    <?php $img = get_all_size_image($thumb_id); ?>
                    <?php $img = $img['thumbnail']; ?>
                  <?php else: ?>
                    <?php $img = get_bloginfo('template_directory') . "/img/thumbnail.jpg"; ?>
                  <?php endif; ?>
                  <div class="col-xs-2">
                      <a href="<?php the_permalink() ?>">
                          <img src="<?php echo $img; ?>" class="img-responsive">
                      </a>
                  </div>
                  <div class="col-xs-10">
                      <h3 class="supermarket" style="margin: 5px 0px;"><a href="<?php the_permalink() ?>" class="red"><?php the_title() ?></a><small> - <?php echo get_the_date(); ?></small></h3>
                      <?php the_excerpt() ?>
                  </div>
              </div>
              <?php wp_reset_postdata(); ?>
            <?php endwhile; ?>
        </div>
    </div>
</div>

<style>
    .drop_arrow{
        position: absolute;
        left: 0;
        right: 0;
        margin: auto;
        bottom: -35px;
        z-index: 10000;
    }
</style>
<?php get_footer(); ?>