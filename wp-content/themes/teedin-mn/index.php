<?php get_header(); ?>

<!--Body index-->
<!--Slide-->
<div class="container" style="margin-top: 20px;">
    <div class="row">
        <div class="col-xs-12">
            <div id="slider" class="nivoSlider">
                <?php $cate_slide = get_posts(["category" => 5, "posts_per_page" => -1]); ?>
                <?php foreach ($cate_slide as $post): setup_postdata($post); ?>
                  <?php $img = get_all_size_image(get_post_thumbnail_id()); ?>
                  <a href="<?php the_permalink() ?>"><img src="<?php echo $img['large']; ?>" alt="" title="<?php the_excerpt() ?>" /></a>
                  <?php wp_reset_postdata(); ?>
                <?php endforeach; ?>
            </div>
            <img src="<?php bloginfo('template_directory'); ?>/img/hilight.png" class="cross_label">
        </div>
    </div>
</div><!--/Slide-->

<div class="blue_ribbon" style="margin-top: 20px;">
    <div class="container" style="position: relative;">
        <div class="row">
            <div class="col-xs-3"><img src="<?php bloginfo('template_directory'); ?>/img/quote_logo.png" class="img-responsive"></div>
            <div class="col-xs-9" style="font-size: 12px;">
                <p style="margin-top: 9px;"><img src="<?php bloginfo('template_directory'); ?>/img/blue_ribbon_label.jpg"></p>
                <?php $post = get_post(25); ?>
                <?php setup_postdata($post); ?>
                <?php the_excerpt(); ?>
                <?php wp_reset_postdata(); ?>
            </div>
        </div>
        <img src="<?php bloginfo('template_directory'); ?>/img/quote_arrow.png" class="drop_arrow">
    </div>
</div>
<style>
    .nivo-controlNav, .nivo-directionNav{
        display: none;
    }

    .cross_label{
        position: absolute;
        z-index: 10000;
        top: 0;
        right: 15px;
    }

    .drop_arrow{
        position: absolute;
        left: 0;
        right: 0;
        margin: auto;
        bottom: -60px;
        z-index: 10000;
    }
</style>
<script type="text/javascript">
  $(function () {
      $('#slider').nivoSlider();
  });
</script><!--/Body index-->

<?php $query_list_cate = [2, 3]; ?>
<div class="container" style="margin-top: 55px;">
    <?php foreach ($query_list_cate as $cate_id): ?>
      <?php $cate_info = get_category($cate_id); ?>
      <h1 class="supermarket bold txt_center blue"><?php echo $cate_info->name; ?></h1>
      <div class="row">
          <div class="col-xs-10 col-xs-offset-1">
              <?php $all_post = get_posts(["category" => $cate_id, "posts_per_page" => 20]); ?>
              <?php foreach ($all_post as $post): setup_postdata($post) ?>
                <div class="row" style="margin-bottom: 20px;">
                    <?php $thumb_id = get_post_thumbnail_id(); ?>
                    <?php if ($thumb_id): ?>
                      <?php $img = get_all_size_image($thumb_id); ?>
                      <?php $img = $img['thumbnail']; ?>
                    <?php else: ?>
                      <?php $img = get_bloginfo('template_directory') . "/img/thumbnail.jpg"; ?>
                    <?php endif; ?>
                    <div class="col-xs-2">
                        <a href="<?php the_permalink() ?>">
                            <img src="<?php echo $img; ?>" class="img-responsive">
                        </a>
                    </div>
                    <div class="col-xs-10">
                        <h3 class="supermarket" style="margin: 5px 0px;"><a href="<?php the_permalink() ?>" class="red"><?php the_title() ?></a><small> - <?php echo get_the_date(); ?></small></h3>
                        <?php the_excerpt() ?>
                    </div>
                </div>
                <?php wp_reset_postdata(); ?>
              <?php endforeach; ?>

              <div class="col-xs-12 txt_right">
                  <a href="<?php echo get_category_link($cate_id); ?>" class="btn btn-primary">ดูทั้งหมด <i class="glyphicon glyphicon-triangle-right"></i></a>
              </div>
          </div>
      </div>
    <?php endforeach; ?>
</div>

<?php get_footer(); ?>