<?php get_header(); ?>
<?php wp_reset_postdata(); ?>
<?php
$concern_cat = get_the_category();
$collected_cat = [];
foreach ($concern_cat as $each_cate) {
  if (strtolower($each_cate->name) != "slide") {
    array_push($collected_cat, $each_cate);
  }
}
?>

<h1 class="blue_ribbon_heaeder txt_center supermarket"><?php the_title(); ?><img src="<?php bloginfo('template_directory'); ?>/img/quote_arrow.png" class="drop_arrow"></h1>


<div class="container" style="margin: 50px auto 40px;">
    <div class="row">
        <div class="col-xs-12">
            <ol class="breadcrumb">
                <li><a href="<?php echo home_url() ?>">หน้าแรก</a></li>
                <li>
                    <?php foreach ($collected_cat as $index => $cate): ?>
                      <?php echo $index != 0 ? ", " : "" ?>
                      <a href="<?php echo get_category_link($cate->term_id) ?>"><?php echo $cate->name ?></a>
                    <?php endforeach; ?>
                </li>
                <li class="active"><?php the_title() ?></li>
            </ol>
        </div>
    </div>

    <div class="row">
        <?php $thumb_id = get_post_thumbnail_id(); ?>
        <?php if ($thumb_id): ?>
          <?php $img = get_all_size_image($thumb_id); ?>
          <?php $img = $img['thumbnail']; ?>
        <?php else: ?>
          <?php $img = get_bloginfo('template_directory') . "/img/thumbnail.jpg"; ?>
        <?php endif; ?>
        <div class="col-xs-2">
            <a href="<?php the_permalink() ?>">
                <img src="<?php echo $img; ?>" class="img-responsive">
            </a>
        </div>
        <div class="col-xs-10">
            <h3 class="supermarket" style="margin: 5px 0px;"><a href="<?php the_permalink() ?>" class="red"><?php the_title() ?></a><small> - <?php echo get_the_date(); ?></small></h3>
            <?php the_excerpt() ?>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <h3 class="supermarket blue">รายละเอียด</h3>
            <?php the_content() ?>
        </div>
    </div>
    <?php $all_raws_img = get_all_post_image(get_the_ID()); ?>
    <?php $all_img = group_obj_by_rows($all_raws_img); ?>
    <div class="row">        
        <div class="col-xs-12">
            <h3 class="supermarket blue">รูปภาพ (<?php echo count($all_raws_img) ?>)</h3>
            <?php foreach ($all_img as $each_row): ?>
              <div class="row">
                  <?php foreach ($each_row as $each_pic): ?>
                    <div class="col-xs-3">
                        <a href="<?php echo $each_pic['large']; ?>" class="thumbnail colorbox" rel="colorbox">
                            <img src="<?php echo $each_pic['medium']; ?>" class="img-responsive">
                        </a>
                    </div>
                  <?php endforeach; ?>
              </div>
            <?php endforeach; ?>
        </div>
    </div>


    <?php $map = get_field('googlemaps') ?>
    <div class="row">
        <div class="col-xs-12">
            <h3 class="supermarket blue">ตำแหน่งในแผนที่</h3>
            <div id="map_canvas">

            </div>
            <a href="<?php echo "http://maps.google.com/?q={$map['lat']},{$map['lng']}" ?>" class="btn btn-primary btn-lg" style="position: absolute; left: 0; right: 0; width: 193px; height: 46px; margin: auto; bottom: -21px;">เปิดใน Google Maps</a>
        </div>
    </div>
</div>

<style>
    .drop_arrow{
        position: absolute;
        left: 0;
        right: 0;
        margin: auto;
        bottom: -35px;
        z-index: 10000;
    }

    #map_canvas{
        height: 450px;
    }
</style>

<script>
  $(function () {
      $('.colorbox').colorbox({maxWidth: '90%', maxHeight: '90%'});
      $('#map_canvas').gmap({
          'mapTypeId': google.maps.MapTypeId.HYBRID,
          'zoom': 17,
          'center': '<?php echo "{$map['lat']},{$map['lng']}" ?>'
      }).bind('init', function (ev, map) {
          $('#map_canvas').gmap('addMarker', {'position': '<?php echo "{$map['lat']},{$map['lng']}" ?>'});
      });
  });
</script>

<?php get_footer(); ?>