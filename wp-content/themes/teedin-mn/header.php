<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" href="<?php bloginfo('template_directory'); ?>/img/favicon.png" type="image/x-icon" />
        <title>
            <?php global $page, $paged; ?>
            <?php wp_title('|', true, 'right'); ?>
            <?php bloginfo('name'); ?>
            <?php $site_description = get_bloginfo('description', 'display'); ?>
            <?php if ($site_description && ( is_home() || is_front_page() )) echo " | $site_description"; ?>
            <?php if ($paged >= 2 || $page >= 2) echo ' | ' . sprintf(__('Page %s', 'twentyeleven'), max($paged, $page)); ?>
        </title>

        <!--CSS Section-->
        <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/bootstrap.min.css">
        <link href='<?php bloginfo('template_directory'); ?>/css/colorbox.css' rel='stylesheet' type='text/css'>    
        <link href='<?php bloginfo('template_directory'); ?>/css/nivoSlider/default/default.css' rel='stylesheet' type='text/css'> 
        <link href='<?php bloginfo('template_directory'); ?>/css/nivoSlider/light/light.css' rel='stylesheet' type='text/css'> 
        <link href='<?php bloginfo('template_directory'); ?>/css/nivoSlider/dark/dark.css' rel='stylesheet' type='text/css'> 
        <link href='<?php bloginfo('template_directory'); ?>/css/nivoSlider/bar/bar.css' rel='stylesheet' type='text/css'>   
        <link href='<?php bloginfo('template_directory'); ?>/css/nivo-slider.css' rel='stylesheet' type='text/css'>    
        <link href='<?php bloginfo('template_directory'); ?>/css/sweet-alert.css' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo('stylesheet_url'); ?>" />

        <!--JS Section-->
        <script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/jquery.min.js"></script>
        <script src="<?php bloginfo('template_directory'); ?>/js/bootstrap.min.js"></script>
        <script src="http://maps.google.com/maps/api/js?sensor=true" type="text/javascript"></script>
        <script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/jquery.colorbox-min.js"></script>
        <script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/jquery.form.js"></script>
        <script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/jquery.nivo.slider.pack.js"></script>
        <script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/jquery.ui.map.js"></script>
        <script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/sweet-alert.min.js"></script>


        <?php if (is_singular() && get_option('thread_comments')): ?>
          <?php wp_enqueue_script('comment-reply'); ?>
        <?php endif; ?>

        <?php wp_head(); ?>

        <style type="text/css">
            /*Responsive disabled*/
            body{
                min-width: 1110px;
            }

            .container {
                width: 1110px;
            }

        </style>
    </head>
    <body  <?php body_class(); ?>>
        <!--Header-->
        <div class="container">
            <div class="row">
                <div class="col-xs-3">
                    <a href="<?php echo home_url(); ?>">
                        <img src="<?php bloginfo('template_directory'); ?>/img/header_logo.png" class="img-responsive">
                    </a>
                </div>
                <div class="col-xs-6 col-xs-offset-3">
                    <div class="row">
                        <div class="col-xs-12">
                            <form role="search" method="get" class="search-form" action="<?php echo home_url('/'); ?>" style="text-align: right; margin-top: 22px;">
                                <img src="<?php bloginfo('template_directory'); ?>/img/search.jpg" style="margin-top: -7px;">
                                <input style="width:310px; display: inline-block; margin: 10px 5px 0px;" type="search" class="search-field form-control" placeholder="<?php echo esc_attr_x('Search …', 'placeholder') ?>" value="<?php echo get_search_query() ?>" name="s" title="<?php echo esc_attr_x('Search for:', 'label') ?>" />
                                <button type="submit" class="search-submit btn btn-primary" value="<?php echo esc_attr_x('Search', 'submit button') ?>" style="margin-top: -4px;">ค้นหา</button>
                            </form>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="menu_head supermarket txt_left">
                                <?php wp_nav_menu(array('theme_location' => 'primary', 'container_class' => 'menu-main-menu-container', 'menu_class' => 'menu clearfix')); ?>
                                <ul class="addon_contact">
                                    <li>
                                        <a href="mailto:bird.songklod@gmail.com"><img src="<?php bloginfo('template_directory'); ?>/img/header_fb_icon.png"></a>
                                    </li>
                                    <li>
                                        <a href="http://www.facebook.com"><img src="<?php bloginfo('template_directory'); ?>/img/header_email_icon.png"></a>
                                    </li>
                                    <li>
                                        <a href="line//:borbird"><img src="<?php bloginfo('template_directory'); ?>/img/header_line_icon.png"></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!--/Header-->